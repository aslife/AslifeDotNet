﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Numerics;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace AslifeApiLib
{
    namespace model
    {
        /**
         * Важность ошибки (Справочник)
         */
        public enum FaultImportance
        {
            /**
             * УСПЕХ. Запрос успешно исполнен и не требует дополнительного внимания пользователя.
             * Может использоваться для передачи технической информации.
             */
            [EnumMember(Value = "notice")] Notice,

            /**
             * УСПЕХ. Запрос исполнен с предупреждением. Требуется дополнительное внимание пользователя или администратора к замечанию.
             */
            [EnumMember(Value = "warning")] Warning,

            /**
             * НЕУДАЧА. Запрос завершился неудачей. Требуется внесение корректировок в запрос.
             */
            [EnumMember(Value = "critical")] Critical
        }

        /**
         * Тип ошибки (Справочник)
         */
        public enum FaultGroup
        {
            /**
             * Бизнес исключения. Возникает при невозможности совершения действия по бизнес-процессу
             */
            [EnumMember(Value = "business_exception")]
            Business,

            /**
             * Ошибка валидации запроса.
             */
            [EnumMember(Value = "validation_exception")]
            Validation,

            /**
             * Ошибка аутентификации пользователя / доступ запрещен
             */
            [EnumMember(Value = "authentication_exception")]
            Authentication,

            /**
             * Системное исключение
             */
            [EnumMember(Value = "system_exception")]
            System
        }

        /**
         * Fault (объект) - информация об ошибке или предупреждение в работе сервиса
         */
        public class Fault
        {
            /**
             * Важность ошибки (Справочник)
             */
            [JsonProperty("importance")]
            [JsonConverter(typeof(StringEnumConverter))]
            public FaultImportance Importance { get; set; }

            /**
             * Тип ошибки (Справочник)
             */
            [JsonProperty("group")]
            [JsonConverter(typeof(StringEnumConverter))]
            public FaultGroup Group { get; set; }

            /**
             * Краткое сообщение о произошедшем
             */
            [JsonProperty("message")] public string Message { get; set; }

            /**
             * Подробное описание исключения (трассировка)
             */
            [JsonProperty("reason")] public string Reason { get; set; }
        }

        public class AsLifeException : Exception
        {
            // public AsLifeException(Fault[] faults)
            // {
            //
            // }

            public AsLifeException(string message) : base(message)
            {
            }
        }

        /**
         * BasicRequest (объект) - Базовый запрос:
         */
        public class BasicRequest<T>
        {
            /**
             * Идентификатор корреляции запроса (используется для асинхронного взаимодействия)
             */
            [JsonProperty("correlation_id")] public string CorrelationId { get; set; }

            /**
             * Полезная нагрузка запроса. Содержит данные необходимые для выполнения запроса
             */
            [JsonProperty("data")] public T Data { get; set; }
        }

        /**
         * BasicResponse (объект) - базовый ответ
         */
        public class BasicResponse<T> : BasicRequest<T>
        {
            /**
             * Информация об ошибках и предупреждения в ходе работы сервиса
             */
            [JsonProperty("faults")] public List<Fault> Faults;
        }

        /**
         * Параметры доступных для оформления продуктов
         */
        public class Product
        {
            /**
             * ИД продукта
             */
            [JsonProperty("id")]
            public Guid Id { get; set; }

            /**
             * 	Код продукта
             */
            [JsonProperty("code")]
            public BigInteger Code { get; set; }

            /**
             * Название продукта
             */
            [JsonProperty("name")]
            public string Name { get; set; }

            /**
             * Иконка продукта
             */
            [JsonProperty("icon")]
            public byte[] Icon { get; set; }

            /**
             * Валюта продукта 
             */
            [JsonProperty("currencies")]
            public string[] Currencies { get; set; }

            /**
             * 	Тип продукта
             */
            [JsonProperty("product_type")]
            public string ProductType { get; set; }
        }

        /**
         * фильтр продуктов
         */
        public class ProductFilter
        {
            /**
             * Логин пользователя в системе агента
             */
            [JsonProperty("agent_employee_id")]
            public string AgentEmployeeId { get; set; }

            /**
             * Тип продукта 
             */
            [JsonProperty("product_types")]
            public string[] ProductTypes { get; set; }
        }

        /**
         * PolicyQualifier (объект) - Идентификаторы полиса
         */
        public class PolicyQualifier
        {
            /**
             * Номер полиса
             */
            [JsonProperty("policy_num")] 
            public string PolicyNum { get; set; }

            /**
             * 	Идентификатор полиса
             */
            [JsonProperty("policy_id")] 
            public BigInteger? PolicyId { get; set; }
        }

        /**
         * ReadPolicy (объект) extends PolicyQualifier - Запрос информации по продукту
         */
        public class ReadPolicy : PolicyQualifier
        {

            /**
             * Логин пользователя в системе агента
             */
            [JsonProperty("agent_employee_id")]
            public string AgentEmployeeId { get; set; }

            /**
             * Код продукта 
             */
            [JsonProperty("product_code")]
            public BigInteger? ProductCode { get; set; }
        }

        /**
         * ProductFieldValue (объект) - Значение поля полиса
         */
        public class ProductFieldValue
        {
            /**
             * Идентификатор поля из ProductField 
             */
            [JsonProperty("id")]
            public string Id { get; set; }

            /**
             * Значение поля
             */
            [JsonProperty("value")]
            public string Value { get; set; }
        }

        /**
         * WritePolicy (объект) - Запись полиса
         */
        public class WritePolicy : PolicyQualifier
        {
            /**
             * Логин пользователя
             */
            [JsonProperty("agent_employee_id")]
            public string EmployeeId { get; set; }

            /**
             * Код продукта
             */
            [JsonProperty("product_code")]
            public BigInteger? ProductCode { get; set; }

            /**
             * Массив данных для полиса
             */
            [JsonProperty("policy_data")]
            public List<ProductFieldValue> PolicyData { get; set; }

        }

        /**
         * ReadDocuments (объект) extends ReadPolicy - Параметры запроса документов по полису
         */
        public class ReadDocuments : ReadPolicy
        {
            /**
             * Флаг, указывающий на то, что тело файл(а/ов) не требуется
             */
            [JsonProperty("without_content")]
            public bool? WithoutContent { get; set; } = false;

            /**
             * Идентифкатор печатной формы для генерации одного файла.
             */
            [JsonProperty("report_id")]
            public Guid? ReportId { get; set; }

        }

        /**
         * Document (объект) - Документ
         */
        public class Document
        {
            /**
             * Идентификатор печатной формы 
             */
            [JsonProperty("report_id")]
            public Guid ReportId { get; set; }

            /**
             * Описание 
             */
            [JsonProperty("doc_desc")]
            public string Description { get; set; }

            /**
             * Название файла
             */
            [JsonProperty("file_name")]
            public string FileName { get; set; }

            /**
             * Содержимое файла в виде BASE64 кодированной строки
             */
            [JsonProperty("file_content")]
            public byte[] FileContent { get; set; }
        }

        /**
         * ApplyPolicyOperation (объект) - Параметры операции с полисом
         */
        public class ApplyPolicyOperation
        {
            /**
             * Логин пользователя
             */
            [JsonProperty("agent_employee_id")]
            public string EmployeeId { get; set; }

            /**
             * Идентификаторы полиса
             */
            [JsonProperty("policy_qualifier")]
             public PolicyQualifier PolicyQualifier { get; set; }

            /**
             * Выбранная операция с полисом
             */
            [JsonProperty("policy_operation")]
            public PolicyOperation Operation { get; set; }
        }

        /**
         * PolicyOperation (объект) - Доступные операции с полисом
         */
        public class PolicyOperation
        {
            /**
             * Код операции полиса
             */
            [JsonProperty("oper_code")]
            public string OperationCode { get; set; }

            /**
             * Код статуса полиса
             */
            [JsonProperty("oper_sub_code")]
            public string OperationSubCode { get; set; }

            /**
             * Название операции полиса
             */
            [JsonProperty("oper_name")]
            public string OperationDescription { get; set; }

            /**
             * Название статуса полиса
             */
            [JsonProperty("oper_sub_name")]
            public string OperationSubDescription { get; set; }
        }

        /**
         * DictionaryEntry (объект) - Запись в справочник
         */
        public class DictionaryEntry
        {
            /**
             * Идентификатор
             */
            [JsonProperty("code")]
            public string Code { get; set; }

            /**
             * Значение
             */
            [JsonProperty("name")]
            public string Name { get; set; }

            /**
             * Подсказка
             */
            [JsonProperty("display_name")]
            public string DisplayName { get; set; }
        }

        /**
         * Dictionary (объект) - Справочник
         */
        public class Dictionary
        {
            /**
             * Название справочника.
             */
            [JsonProperty("name")]
            public string Name { get; set; }

            /**
             * Записи справочника.
             */
            [JsonProperty("entries")]
            public List<DictionaryEntry> Entries { get; set; }
        }

        /**
         * ProductField (объект) - Поле продукта
         */
        public class ProductField : ProductFieldValue
        {
            /**
            * Тип данных поля
            */
            [JsonProperty("data_type")]
            public string DataType { get; set; }

            /**
             * Максимально допустимое количество символов в значении поля
             */
            [JsonProperty("data_length")]
            public int? DataLength { get; set; }

            /**
             * Название поля
             */
            [JsonProperty("name")]
            public string Name { get; set; }

            /**
             * Название поля на русском языке
             */
            [JsonProperty("caption")]
            public string Caption { get; set; }

            /**
             * Подсказка к полю на русском языке
             */
            [JsonProperty("hint")]
            public string Hint { get; set; }

            /**
             * Название справочника. Когда не пустое, возможные значения брать из справочника.
             */
            [JsonProperty("dict_code")]
            public string DictCode;

            /**
             * Флаг обязательности поля
             */
            [JsonProperty("required")]
            public bool Required { get; set; }

            /**
             * 	Флаг необходимости отображения поля на форме
             */
            [JsonProperty("visible")]
            public bool Visible { get; set; }

            /**
             * Флаг возможности редактирования поля
             */
            [JsonProperty("readonly")]
            public bool ReadOnly { get; set; }

        }

        /**
         * ProductEntity (объект) - Элемент структуры продукта
         */
        public class ProductEntity
        {
            /**
             * Название элемента структуры.
             */
            [JsonProperty("code")]
            public string Code { get; set; }

            /**
             * Тип элемента структуры.
             */
            [JsonProperty("type")]
            public string Type { get; set; }

            /**
             * Название элемента структуры на русском.
             */
            [JsonProperty("description")]
            public string Description { get; set; }

            /**
             * Дочерние элементы структуры.
             */
            [JsonProperty("children")]
            public List<ProductEntity> Children { get; set; }

            /**
             * Поля элемента структуры.
             */
            [JsonProperty("fields")]
            public Collection<ProductField> Fields { get; set; }
        }

        /**
         * ProductStruct (объект) - Структура продукта
         */
        public class ProductStruct
        {
            /**
             * Справочники
             */
            [JsonProperty("dictionaries")]
            public List<Dictionary> Dictionaries { get; set; }

            /**
             * Динамический объект продукта
             */
            [JsonProperty("entities")]
            public ProductEntity Entities { get; set; }

        }

        /**
         * PolicyState (объект) - Статус полиса
         */
        public class PolicyState
        {
            /**
             * ID статуса
             */
            [JsonProperty("state_code")]
            public int State { get; set; }

            /**
             * ID подстатуса
             */
            [JsonProperty("ext_state_code")]
            public int ExtState { get; set; }

            /**
             * 	Описание статуса
             */
            [JsonProperty("state_desc")]
            public string StateDescription { get; set; }

            /**
             * Описание подстатуса
             */
            [JsonProperty("ext_state_desc")]
            public string ExtStateDescription { get; set; }
        }

        /**
         * PolicyInfoBase (объект) - Базовая информация по полису
         */
        public class PolicyInfoBase : PolicyQualifier
        {
            /**
             * Код продукта
             */
            [JsonProperty("insr_type")]
            public BigInteger InsrType { get; set; }

            /**
             * ФИО агента продавшего полис
             */
            [JsonProperty("agent_name")]
            public string AgentName { get; set; }

            /**
             * ФИО купившего полис
             */
            [JsonProperty("client_fullname")]
            public string ClientFillname { get; set; }

            /**
             * Логин агента продавшего полис
             */
            [JsonProperty("user_info")]
            public string UserLogin { get; set; }

            /**
             * Тип подписания документа (Для ЭДО)
             */
            [JsonProperty("signature_type")]
            public string SignatureType { get; set; }

            /**
             * Статус полиса
             */
            [JsonProperty("state")]
            public PolicyState PolicyState { get; set; }
        }

    }
}