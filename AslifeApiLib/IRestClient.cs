﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AslifeApiLib.model;
using Newtonsoft.Json;

namespace AslifeApiLib
{
    public interface IRestClient : IDisposable
    {
        /**
         * GetProducts - Получение списка доступных для оформления продуктов
         */
        BasicResponse<List<Product>> GetProducts(ProductFilter request);

        /**
         * ReadProduct - Получение информации по продукту
         */
        BasicResponse<ProductStruct> ReadProduct(ReadPolicy request);

        /**
         * WriteProduct - Запись/изменение информации по продукту
         */
        BasicResponse<PolicyQualifier> WriteProduct(WritePolicy request);

        /**
         * DocumentsToPrint - получение документов
         */
        BasicResponse<List<Document>> DocumentsToPrint(ReadDocuments request);

        /**
         * PolicyOperations - получение списка операций для полиса
         */
        BasicResponse<List<PolicyOperation>> PolicyOperations(ReadPolicy request);

        /**
         * ApplyPolicyOperation- запрос на проведение операции с полисом
         */
        BasicResponse<List<PolicyOperation>> ApplyPolicyOperation(ApplyPolicyOperation request);

        /**
         * PolicyBaseInfo - запрос базовой информации по полису
         */
        BasicResponse<PolicyInfoBase> PolicyBaseInfo(ReadPolicy request);
    }


    public class RestClient : IRestClient, IDisposable
    {
        protected readonly string _endpoint;
        protected readonly HttpClient _client;

        protected RestClient(string endpoint, string login, string password)
        {
            _endpoint = (endpoint.EndsWith("/") ? endpoint.Substring(0, endpoint.Length - 2) : endpoint);
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!string.IsNullOrWhiteSpace(login))
            {
                var credentials = $"{login}:{password}";
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(Encoding.ASCII.GetBytes(credentials)));
            }
        }

        public void Dispose()
        {
            _client?.Dispose();
        }

        /**
         *
         */
        protected BasicResponse<TRs> Call<TRq, TRs>(TRq request, string path)
        {
            var rqContainer = new BasicRequest<TRq>
            {
                CorrelationId = new Guid().ToString(),
                Data = request
            };

            try
            {
                var content = new StringContent(JsonConvert.SerializeObject(rqContainer, Formatting.Indented),
                    Encoding.UTF8, "application/json");
                var url = $"{_endpoint}{path}";
                var rsHttpTask = _client.PostAsync(url, content);
                rsHttpTask.Wait(30000);

                var rsStrTask = rsHttpTask.Result.Content.ReadAsStringAsync();
                rsStrTask.Wait(30000);
                return JsonConvert.DeserializeObject<BasicResponse<TRs>>(rsStrTask.Result);
            }
            catch (Exception ex)
            {
                return new BasicResponse<TRs>{Faults = new List<Fault>
                {
                    new Fault
                    {
                        Importance = FaultImportance.Critical,
                        Group = FaultGroup.System,
                        Message = $"Непредвиденная ошибка исполнения запроса. {ex.Message}",
                        Reason = ex.ToString()
                    }
                }};
            }
        }

        /**
         *
         */
        public static IRestClient Create(string endpoint, string login, string password)
        {
            return new RestClient(endpoint, login, password);
        }

        public BasicResponse<List<Product>> GetProducts(ProductFilter request)
        {
            return Call<ProductFilter, List<Product>>(request, "/product/list");
        }

        public BasicResponse<ProductStruct> ReadProduct(ReadPolicy request)
        {
            return Call<ReadPolicy, ProductStruct>(request, "/product/read");
        }

        public BasicResponse<PolicyQualifier> WriteProduct(WritePolicy request)
        {
            return Call<WritePolicy, PolicyQualifier>(request, "/product/write");
        }

        public BasicResponse<List<Document>> DocumentsToPrint(ReadDocuments request)
        {
            return Call<ReadDocuments, List<Document>>(request, "/product/documents_to_print_ext");
        }

        public BasicResponse<List<PolicyOperation>> PolicyOperations(ReadPolicy request)
        {
            return Call<ReadPolicy, List<PolicyOperation>>(request, "/product/policy_operations");
        }

        public BasicResponse<List<PolicyOperation>> ApplyPolicyOperation(ApplyPolicyOperation request)
        {
            return Call<ApplyPolicyOperation, List<PolicyOperation>>(request, "/product/apply_policy_operation");
        }

        public BasicResponse<PolicyInfoBase> PolicyBaseInfo(ReadPolicy request)
        {
            return Call<ReadPolicy, PolicyInfoBase>(request, "/policy/base_info");
        }
    }
}