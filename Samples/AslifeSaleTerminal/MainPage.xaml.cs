﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using AslifeApiLib.model;
using AslifeSaleTerminal.UI.Pages;
using AslifeSaleTerminal.UI.Primitives;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace AslifeSaleTerminal
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private NavigationViewItem _lastItem;

        public MainPage()
        {
            this.InitializeComponent();
            var goBack = new KeyboardAccelerator {Key = VirtualKey.GoBack};
            goBack.Invoked += BackInvoked;
            var altLeft = new KeyboardAccelerator {Key = VirtualKey.Left};
            altLeft.Invoked += BackInvoked;
            this.KeyboardAccelerators.Add(goBack);
            this.KeyboardAccelerators.Add(altLeft);
            altLeft.Modifiers = VirtualKeyModifiers.Menu;
            Navi.BackRequested += (sender, args) => On_BackRequested();
            ContentFrame.Navigated += (sender, args) => UpdateBackButton();
            UpdateBackButton();
        }

        private void UpdateBackButton()
        {
            Navi.IsBackEnabled = ContentFrame.CanGoBack;
            Navi.IsBackButtonVisible = ContentFrame.CanGoBack
                ? NavigationViewBackButtonVisible.Auto
                : NavigationViewBackButtonVisible.Collapsed;
        }

        private bool On_BackRequested()
        {
            if (!ContentFrame.CanGoBack) return false;
            ContentFrame.GoBack();
            UpdateBackButton();
            return true;
        }

        private void BackInvoked(KeyboardAccelerator sender, KeyboardAcceleratorInvokedEventArgs args)
        {
            On_BackRequested();
            args.Handled = true;
        }

        private void NavigationView_OnItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            // if (!(args.InvokedItemContainer is NavigationViewItem item) || item == _lastItem)
            // return;

            var item = (NavigationViewItem)args.InvokedItemContainer;
            
            var clickedView = args.IsSettingsInvoked ? "SettingsPage" : item.Tag?.ToString() ?? "";
            if (!NavigateToView(clickedView)) return;
            _lastItem = item;
        }

        private bool NavigateToView(string clickedView)
        {
            if (string.IsNullOrWhiteSpace(clickedView))
            {
                ContentFrame.Content = "";
                ContentFrame.BackStack.Clear();
                UpdateBackButton();
                return true;
            }

            var view = Assembly.GetExecutingAssembly()
                .GetType($"AslifeSaleTerminal.UI.Pages.{clickedView}");

            if (view == null)
                return false;

            ContentFrame.Navigate(view, null, new EntranceNavigationTransitionInfo());
            ContentFrame.BackStack.Clear();
            UpdateBackButton();
            return true;
        }

        public static void ClearHistory(params PageStackEntry[] entries)
        {
            var frame = (Frame) Window.Current.Content;
            var page = (MainPage) frame?.Content;
            page?.ContentFrame?.BackStack.Clear();
            if (null == entries) return;
            foreach (var pageStackEntry in entries)
            {
                page?.ContentFrame?.BackStack.Add(pageStackEntry);
            }
            page?.UpdateBackButton();
        }

    }
}