﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace AslifeSaleTerminal.Converters
{
    public class ImagePathConverter : IValueConverter
    {

        public static BitmapImage Convert(object value)
        {
            return new BitmapImage(new Uri($"ms-appx:///Assets/fault/{value}.png"));
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return Convert(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}