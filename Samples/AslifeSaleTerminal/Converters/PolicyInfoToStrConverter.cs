﻿using System;
using Windows.UI.Xaml.Data;
using AslifeApiLib.model;

namespace AslifeSaleTerminal.Converters
{
    public class PolicyInfoToStrConverter : IValueConverter
    {
        public static string Convert(PolicyInfoBase policyInfo)
        {
            if (null == policyInfo)
                return "";

            return $"Идентификатор полиса: {policyInfo.PolicyId}\n"
                   + $"Номер полиса: {policyInfo.PolicyNum}\n"
                   + $"Код продукта: {policyInfo.InsrType}\n"
                   + $"ФИО клиента: {policyInfo.ClientFillname}\n"
                   + $"Статус полиса: {policyInfo.PolicyState.ExtStateDescription}, {policyInfo.PolicyState.ExtStateDescription}\n"
                   + $"Тип подписания: {policyInfo.SignatureType}\n"
                   + "\n"
                   + $"ФИО агента: {policyInfo.AgentName}\n"
                   + $"Логин агента: {policyInfo.AgentName}\n"
                ;
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return Convert(value as PolicyInfoBase);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}