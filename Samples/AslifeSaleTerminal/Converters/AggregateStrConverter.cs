﻿using System;
using System.Linq;
using Windows.UI.Xaml.Data;

namespace AslifeSaleTerminal.Converters
{
    public class AggregateStrConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value.GetType().IsArray ? ((object[])value).Aggregate((i, j) => i + ", " + j) : value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return null;
        }

    }
}