﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;
using AslifeApiLib;
using AslifeApiLib.model;

namespace AslifeSaleTerminal
{
    public class Util
    {
        public enum AgentEmployeeIdProvider
        {
            CurrentUser,
            CurrentUserWithoutDomain,
            CustomValue
        }

        private const string PropNameServiceEndpoint = "api_service_endpoint";
        private const string PropNameServiceLogin = "api_service_login";
        private const string PropNameServicePassword = "api_service_password";
        private const string PropNameEmployeeIdProvider = "agent_employee_id_provider";
        private const string PropNameCustomEmployeeId = "agent_custon_employee_id";

        private static readonly ApplicationDataContainer LocalSettings = ApplicationData.Current.LocalSettings;
        private static readonly Regex RxDomainUser = new Regex("^.+\\\\(?<login>.+)$");

        public static string ServiceEndpoint
        {
            get => LocalSettings.Values[PropNameServiceEndpoint] as string ??
                   "https://oapi-test.aslife.ru/internal/policy_api";
            set => LocalSettings.Values[PropNameServiceEndpoint] = value;
        }

        public static string ServiceLogin
        {
            get => LocalSettings.Values[PropNameServiceLogin] as string;
            set => LocalSettings.Values[PropNameServiceLogin] = value;
        }

        public static string ServicePassword
        {
            get => LocalSettings.Values[PropNameServicePassword] as string;
            set => LocalSettings.Values[PropNameServicePassword] = value;
        }

        public static AgentEmployeeIdProvider EmployeeIdProvider
        {
            get
            {
                var rawValue = LocalSettings.Values[PropNameEmployeeIdProvider];
                if (null == rawValue)
                    return AgentEmployeeIdProvider.CurrentUserWithoutDomain;
                return (AgentEmployeeIdProvider) rawValue;
            }
            set => LocalSettings.Values[PropNameEmployeeIdProvider] = (int) value;
        }

        public static string AgentEmployeeId
        {
            get =>
                GetAgentEmployeeId(EmployeeIdProvider,
                    LocalSettings.Values[PropNameCustomEmployeeId] as string);
            set => LocalSettings.Values[PropNameCustomEmployeeId] = value;
        }

        public static string GetAgentEmployeeId(AgentEmployeeIdProvider provider, string customValue)
        {
            switch (provider)
            {
                case AgentEmployeeIdProvider.CurrentUser:
                    return System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                case AgentEmployeeIdProvider.CurrentUserWithoutDomain:
                    var currentUserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                    var matcher = RxDomainUser.Match(currentUserName);
                    return !matcher.Success ? currentUserName : matcher.Groups["login"].Value;
                case AgentEmployeeIdProvider.CustomValue:
                    return customValue ?? "";
                default:
                    throw new AsLifeException("Недопустимый тип провайдера значений EmployeeId");
            }
        }

        public static IRestClient ApiClient => RestClient.Create(ServiceEndpoint, ServiceLogin, ServicePassword);
    }
}