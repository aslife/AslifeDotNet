﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using AslifeApiLib.model;


namespace AslifeSaleTerminal.UI.Primitives
{
    public sealed partial class ProductForm : UserControl
    {
        public const string FormatDate = "dd.MM.yyyy";

        private static readonly Regex NumRegex = new Regex("[^0-9]+");

        private readonly Dictionary<string, List<DictionaryEntry>> _dictionaries =
            new Dictionary<string, List<DictionaryEntry>>();

        private List<ProductField> _fields;

        public ProductForm()
        {
            this.InitializeComponent();
        }

        public void Clear()
        {
            Form.Children.Clear();
            _dictionaries.Clear();
            _fields = new List<ProductField>();
        }

        public List<ProductFieldValue> GetValues()
        {
            return _fields.ConvertAll(pf => new ProductFieldValue {Id = pf.Id, Value = pf.Value});
        }

        public void SetFields(ProductEntity rootEntity, List<Dictionary> dictionaries)
        {
            Clear();
            dictionaries.ForEach(dict => _dictionaries.Add(dict.Name, dict.Entries));
            ProductEntityProcessor(rootEntity);
        }

        private void ProductEntityProcessor(ProductEntity entity, string patentPath = "")
        {
            var path = (string.IsNullOrWhiteSpace(patentPath)
                ? entity.Description
                : $"{patentPath}/{entity.Description}");
            if (null != entity.Fields && 0 != entity.Fields.Count)
            {
                if (path.EndsWith("/Человек"))
                    path = path.Substring(0, path.Length - 8);

                var blockTitle = new TextBlock
                {
                    Margin = new Thickness(0, 7, 0, 0),
                    Text = "-- " + path + ": ",
                    Style = Resources["TitleTextBlockStyle"] as Style
                };
                Form.Children.Add(blockTitle);
                var isBlockEmpty = true;
                foreach (var field in entity.Fields)
                {
                    var fieldUi = CreateField(field);
                    if (null == fieldUi) continue;
                    Form.Children.Add(fieldUi);
                    isBlockEmpty = false;
                }

                if (isBlockEmpty)
                    Form.Children.Remove(blockTitle);
            }

            if (null == entity.Children || 0 == entity.Children.Count) return;
            foreach (var entityChild in entity.Children)
            {
                ProductEntityProcessor(entityChild, path);
            }
        }

        private Control CreateField(ProductField field)
        {
            _fields.Add(field);

            if (!field.Visible)
            {
                return null;
            }

            var splitter = (field.Required ? "*: " : ": ");

            Control control = null;
            if (!string.IsNullOrWhiteSpace(field.DictCode))
            {
                var combo = new ComboBox {DisplayMemberPath = "DisplayName", Header = field.Caption + splitter, IsEnabled = !field.ReadOnly };

                var dict = !_dictionaries.ContainsKey(field.DictCode)
                    ? new List<DictionaryEntry>()
                    : new List<DictionaryEntry>(_dictionaries[field.DictCode]);
                if (!field.Required)
                    dict.Insert(0, new DictionaryEntry {DisplayName = "", Name = "", Code = ""});

                combo.ItemsSource = dict;
                if (null != field.Value)
                    combo.SelectedItem = _dictionaries[field.DictCode]
                        .Find(de => field.Value == de.Code);
                else if (combo.Items != null && 0 != combo.Items.Count)
                    combo.SelectedIndex = 0;

                combo.SelectionChanged += (sender, args) => 
                    field.Value = ((DictionaryEntry) combo.SelectedItem)?.Code;

                control = combo;
            }
            else
            {
                switch (field.DataType)
                {
                    case "TEXT":
                    case "VARCHAR":
                    case "NVARCHAR":
                    case "VARCHAR2":
                    case "NVARCHAR2":
                    {
                        var tb = new TextBox
                        {
                            Header = field.Caption + splitter,
                            MaxLength = field.DataLength ?? int.MaxValue,
                            IsReadOnly = field.ReadOnly
                        };
                        if (null != field.Value)
                            tb.Text = field.Value;
                        tb.TextChanged += (sender, args) => field.Value = tb.Text;
                        control = tb;
                        break;
                    }
                    case "NUMBER":
                    case "INT":
                    case "INTEGER":
                    case "LONG":
                    case "SHORT":
                    case "BYTE":
                    {
                        var tb = new TextBox {Header = field.Caption + splitter, IsReadOnly = field.ReadOnly};
                        if (null != field.Value)
                            tb.Text = field.Value;
                        tb.TextChanged += (sender, args) => field.Value = tb.Text;
                        tb.BeforeTextChanging += (sender, args) => args.Cancel = NumRegex.IsMatch(args.NewText);
                        control = tb;
                        break;
                    }
                    case "DATE":
                    case "DATETIME":
                    {
                        var dp = new DatePicker {Header = field.Caption + splitter, IsEnabled = !field.ReadOnly};
                        if (null != field.Value)
                            dp.SelectedDate =
                                DateTime.ParseExact(field.Value, FormatDate, CultureInfo.InvariantCulture);
                        dp.SelectedDateChanged += (sender, args) => 
                            field.Value = dp.SelectedDate?.ToString(FormatDate);
                        control = dp;
                        break;
                    }
                    // default:
                    // var dialog = new MessageDialog($"Обнаружен неподдерживаемый тип поля: {field.DataType}");
                    // return null;
                }
            }

            if (null != control)
                ToolTipService.SetToolTip(control, field.Hint);
            return control;
        }
    }
}