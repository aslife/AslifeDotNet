﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Windows.UI.Xaml.Controls;
using AslifeApiLib.model;
using AslifeSaleTerminal.Converters;


namespace AslifeSaleTerminal.UI.Primitives
{
    public sealed partial class FaultDialog : ContentDialog
    {
        public ObservableCollection<Fault> Faults { get; }

        public FaultDialog(IEnumerable<Fault> faults) : this()
        {
            foreach (var fault in faults)
            {
                Faults.Add(fault);
            }
        }


        public FaultDialog()
        {
            Faults = new ObservableCollection<Fault>();
            InitializeComponent();
            Faults.CollectionChanged += FaultsOnCollectionChanged;
            Opened += (sender, args) =>
            {
                if (null != Faults && 0 != Faults.Count)
                    FaultListView.SelectedIndex = 0;
            };
        }

        private void FaultsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ImportanceIcon.Source = (0 == Faults.Count
                ? null
                : ImagePathConverter.Convert(Faults.Max(fault => fault.Importance)));
        }


        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }

        private void FaultList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = ((Fault) FaultListView.SelectedItem);
            DetailTextBox.Text = (null == selected
                    ? ""
                    : $"[ {selected.Group} | {selected.Importance} ] {selected.Message}\n==============================\n\n{selected.Reason}"
                );
        }
    }
}