﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using AslifeApiLib.model;
using AslifeSaleTerminal.UI.Primitives;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AslifeSaleTerminal.UI.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ProductListPage : Page
    {
        public ObservableCollection<Product> Products { get; } = new ObservableCollection<Product>();

        public ProductListPage()
        {
            this.InitializeComponent();
            ButtonReload.Click += (sender, args) => Load();
        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Load();
        }

        public async void Load()
        {
            Products.Clear();
            Preloader.IsActive = true;

            try
            {
                using (var client = Util.ApiClient)
                {
                    var result = await Task.Run(() =>
                        client.GetProducts(new ProductFilter {AgentEmployeeId = Util.AgentEmployeeId}));
                    if (null != result.Faults && 0 != result.Faults.Count)
                        await new FaultDialog(result.Faults).ShowAsync();
                    else
                        result.Data.ForEach(product => Products.Add(product));
                }
            }
            finally
            {
                Preloader.IsActive = false;
            }
        }

        private void Selector_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = (Product) ProductListView.SelectedItem;
            if (null == selected)
                return;
            Frame.Navigate(typeof(ProductPage), new ReadPolicy {ProductCode = selected.Code});
        }
    }
}