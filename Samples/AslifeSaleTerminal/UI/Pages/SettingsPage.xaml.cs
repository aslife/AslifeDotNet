﻿using System;
using System.Linq;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using AslifeApiLib;
using AslifeApiLib.model;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AslifeSaleTerminal.UI.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        public SettingsPage()
        {
            this.InitializeComponent();

            ButtonReload.Click += (sender, args) => Reload();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Reload();
        }

        private void Reload()
        {
            ServiceEndpointField.Text = Util.ServiceEndpoint ?? "";
            ServiceLoginField.Text = Util.ServiceLogin ?? "";
            ServicePasswordField.Password = Util.ServicePassword ?? "";
            SelectedEmployeeIdProvider = Util.EmployeeIdProvider;
            CustomEmployeeIdField.Text = Util.AgentEmployeeId ?? "";
        }

        private void CustomEmployeeId_OnChecked(object sender, RoutedEventArgs e)
        {
            CustomEmployeeIdField.Visibility = Visibility.Visible;
        }

        private void CustomEmployeeId_OnUnchecked(object sender, RoutedEventArgs e)
        {
            CustomEmployeeIdField.Visibility = Visibility.Collapsed;
        }


        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Util.ServiceEndpoint = ServiceEndpointField.Text;
            Util.ServiceLogin = ServiceLoginField.Text;
            Util.ServicePassword = ServicePasswordField.Password;
            Util.EmployeeIdProvider = SelectedEmployeeIdProvider;
            Util.AgentEmployeeId = CustomEmployeeIdField.Text;
        }

        public Util.AgentEmployeeIdProvider SelectedEmployeeIdProvider
        {
            get
            {
                if (null != EmplOSRadio.IsChecked && EmplOSRadio.IsChecked.Value)
                    return Util.AgentEmployeeIdProvider.CurrentUser;
                if (null != EmplOSWithoutDomainRadio.IsChecked && EmplOSWithoutDomainRadio.IsChecked.Value)
                    return Util.AgentEmployeeIdProvider.CurrentUserWithoutDomain;
                if (null != EmplCustomRadio.IsChecked && EmplCustomRadio.IsChecked.Value)
                    return Util.AgentEmployeeIdProvider.CustomValue;

                return Util.AgentEmployeeIdProvider.CurrentUserWithoutDomain;
            }

            set
            {
                switch (value)
                {
                    case Util.AgentEmployeeIdProvider.CurrentUser:
                        EmplOSRadio.IsChecked = true;
                        break;
                    case Util.AgentEmployeeIdProvider.CustomValue:
                        EmplCustomRadio.IsChecked = true;
                        break;
                    default:
                        EmplOSWithoutDomainRadio.IsChecked = true;
                        break;
                }
            }
        }

        private async void TestButton_OnClick(object sender, RoutedEventArgs e)
        {
            Preloader.IsActive = true;
            this.IsEnabled = false;
            try
            {
                using (var client = RestClient.Create(ServiceEndpointField.Text, ServiceLoginField.Text,
                    ServicePasswordField.Password))
                {
                    var response = client.GetProducts(new ProductFilter
                    {
                        AgentEmployeeId =
                            Util.GetAgentEmployeeId(SelectedEmployeeIdProvider, CustomEmployeeIdField.Text)
                    });
                    if (null != response.Faults && 0 != response.Faults.Count)
                    {
                        var maxFault = response.Faults.Find(fault =>
                            fault.Importance == response.Faults.Max(f => f.Importance));

                        throw new AsLifeException(
                            "Запрос к серверу успешно исполнен, " +
                            $"но полученные данные содержат информацию об ошибках.\n{maxFault.Message}");
                    }

                    var dialog =
                        new MessageDialog(
                            "Проврка завершилась успехом",
                            "Успех");
                    await dialog.ShowAsync();
                }
            }
            catch (Exception ex)
            {
                var dialog =
                    new MessageDialog(
                        $"Проврка завершилась неудачей.\n{ex.Message}",
                        "Ошибка");
                await dialog.ShowAsync();
            }
            finally
            {
                Preloader.IsActive = false;
                this.IsEnabled = true;
            }
        }
    }
}