﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Numerics;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using AslifeApiLib.model;
using AslifeSaleTerminal.UI.Primitives;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AslifeSaleTerminal.UI.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /**
     * Страница полиса
     */
    public sealed partial class PolicyPage : Page
    {
        /**
         * Регулярное выражение для получения расширения файла
         */
        private readonly Regex _fileExt = new Regex("^.+(?<extGroup>\\..+)");

        private PolicyQualifier _qualifier;

        public PolicyInfoBase PolicyInfo { get; private set; } = null;

        public ObservableCollection<Document> Documents { get; } = new ObservableCollection<Document>();

        public PolicyPage()
        {
            this.InitializeComponent();
            ButtonReload.Click += (sender, args) => Reload();
        }

        /**
         * Инициализация объекта для передачи его в запросы информации по полису
         */
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e?.Parameter is PolicyQualifier qualifier)
                _qualifier = qualifier;

            Reload();
        }

        /**
         * Перезагрузка страницы
         */
        public async void Reload()
        {
            if (null == _qualifier)
            {
                var combo = new ComboBox
                {
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    Items = {"Номер полиса", "Идентификатор полиса"},
                    SelectedIndex = 0,
                    Margin = new Thickness(3)
                };
                var value = new TextBox {Margin = new Thickness(3)};
                var dialog = new ContentDialog
                {
                    Content = new StackPanel
                    {
                        Orientation = Orientation.Vertical,
                        Children =
                        {
                            combo,
                            value
                        }
                    },
                    CloseButtonText = "Отмена",
                    PrimaryButtonText = "OK"
                };

                if (ContentDialogResult.Primary != await dialog.ShowAsync())
                    return;

                _qualifier = new ReadPolicy();
                if (0 == combo.SelectedIndex)
                    _qualifier.PolicyNum = value.Text.Trim();
                else if (BigInteger.TryParse(value.Text.Trim(), out var policyId))
                    _qualifier.PolicyId = policyId;
                else
                    return;
            }

            Preloader.IsActive = true;
            try
            {
                if (null == _qualifier) return;
                await LoadBaseInfo();
                await LoadOperations();
                await LoadDocuments();
            }
            finally
            {
                Preloader.IsActive = false;
            }
        }

        /**
         * Подгрузка базовой информации по полису
         */
        private async Task LoadBaseInfo()
        {
            PolicyInfo = null;
            PolicyInfoText.Text = "";

            BasicResponse<PolicyInfoBase> result;
            using (var client = Util.ApiClient)
            {
                result = await Task.Run(() => client.PolicyBaseInfo(new ReadPolicy
                {
                    AgentEmployeeId = Util.AgentEmployeeId,
                    PolicyNum = _qualifier.PolicyNum,
                    PolicyId = _qualifier.PolicyId
                }));
            }

            PolicyInfo = await CheckFaults(result);

            if (null == PolicyInfo)
            {
                PolicyInfoText.Text = "НЕТ ДАННЫХ";
                return;
            }

            PolicyInfoText.Text = $"Идентификатор полиса: {PolicyInfo.PolicyId}\n"
                                  + $"Номер полиса: {PolicyInfo.PolicyNum}\n"
                                  + $"Код продукта: {PolicyInfo.InsrType}\n"
                                  + $"ФИО клиента: {PolicyInfo.ClientFillname}\n"
                                  + $"Статус полиса: {PolicyInfo.PolicyState.ExtStateDescription}, {PolicyInfo.PolicyState.ExtStateDescription}\n"
                                  + $"Тип подписания: {PolicyInfo.SignatureType}\n"
                                  + "\n"
                                  + $"ФИО агента: {PolicyInfo.AgentName}\n"
                                  + $"Логин агента: {PolicyInfo.AgentName}\n"
                ;
        }

        /**
         * Подгрузка доступных операций
         */
        private async Task LoadOperations(List<PolicyOperation> operations = null)
        {
            CommandPanel.Children.Clear();

            if (null == operations)
            {
                using (var client = Util.ApiClient)
                {
                    var result = await Task.Run(() => client.PolicyOperations(new ReadPolicy
                    {
                        AgentEmployeeId = Util.AgentEmployeeId,
                        PolicyNum = _qualifier.PolicyNum,
                        PolicyId = _qualifier.PolicyId
                    }));
                    operations = await CheckFaults(result);
                }
            }

            operations.ForEach(operation =>
            {
                AppBarButton btn;
                CommandPanel.Children.Add(btn = new AppBarButton
                    {Content = operation.OperationSubDescription, Width = 130});
                btn.Click += async (sender, args) => await RunOperation(operation);
            });
        }

        /**
         * Подгрузка документов по полису
         */
        private async Task LoadDocuments()
        {
            Documents.Clear();

            BasicResponse<List<Document>> result;
            using (var client = Util.ApiClient)
            {
                result = await Task.Run(() => client.DocumentsToPrint(new ReadDocuments
                {
                    AgentEmployeeId = Util.AgentEmployeeId,
                    PolicyNum = _qualifier.PolicyNum,
                    PolicyId = _qualifier.PolicyId,
                    WithoutContent = true
                }));
            }

            var resultData = await CheckFaults(result);
            resultData?.ForEach(document => Documents.Add(document));
        }

        /**
         * Выполнение операции с полисом
         */
        private async Task RunOperation(PolicyOperation operation)
        {
            if (null == operation)
                return;

            var dialog =
                new MessageDialog(
                    $"Вы действительно хотите исполнить опреацию\n{operation.OperationSubDescription}?",
                    "Подтверждение");
            var confirmCommand = new UICommand("Да");
            var cancelCommand = new UICommand("Нет");
            dialog.Commands.Add(confirmCommand);
            dialog.Commands.Add(cancelCommand);
            if (await dialog.ShowAsync() == cancelCommand)
                return;

            this.IsEnabled = false;
            Preloader.IsActive = true;

            try
            {
                BasicResponse<List<PolicyOperation>> result;
                using (var client = Util.ApiClient)
                {
                    result = await Task.Run(() => client.ApplyPolicyOperation(new ApplyPolicyOperation
                    {
                        EmployeeId = Util.AgentEmployeeId,
                        PolicyQualifier = new PolicyQualifier
                        {
                            PolicyNum = PolicyInfo.PolicyNum,
                            PolicyId = PolicyInfo.PolicyId
                        },
                        Operation = operation
                    }));
                }

                var operations = await CheckFaults(result);

                if (null == result.Faults || 0 == result.Faults.Count)
                {
                    var msg = new MessageDialog("Операция успешно исполнена.");
                    await msg.ShowAsync();
                }

                await LoadOperations(operations);
            }
            finally
            {
                this.IsEnabled = true;
                Preloader.IsActive = false;
            }
        }

        /**
         * Отображение ошибок
         */
        private async Task<Trs> CheckFaults<Trs>(BasicResponse<Trs> result)
        {
            if (null != result.Faults && 0 != result.Faults.Count)
                await new FaultDialog(result.Faults).ShowAsync();

            return result.Data;
        }

        /**
         * Обработка выбранного документа
         */
        private async void DocumentListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = (Document) DocumentListView.SelectedItem;
            if (null == selected)
                return;


            Preloader.IsActive = true;
            this.IsEnabled = false;
            try
            {
                BasicResponse<List<Document>> result;
                using (var client = Util.ApiClient)
                {
                    result = await Task.Run(() => client.DocumentsToPrint(new ReadDocuments
                    {
                        AgentEmployeeId = Util.AgentEmployeeId,
                        PolicyNum = _qualifier.PolicyNum,
                        PolicyId = _qualifier.PolicyId,
                        ReportId = selected.ReportId,
                        WithoutContent = false
                    }));
                }

                var docList = await CheckFaults(result);
                if (0 == docList.Count)
                {
                    var msg = new MessageDialog("Не удалось получить документ.");
                    await msg.ShowAsync();
                }

                var document = docList[0];

                var extension = ".pdf";

                var match = _fileExt.Match(selected.FileName);
                if (match.Success)
                    extension = match.Groups["extGroup"].Value;

                var savePicker = new FileSavePicker
                {
                    SuggestedFileName = document.FileName,
                    SuggestedStartLocation = PickerLocationId.Downloads
                };
                savePicker.FileTypeChoices.Add("AsLife Document", new List<string> {extension});
                var file = await savePicker.PickSaveFileAsync();
                if (null == file)
                    return;

                await FileIO.WriteBytesAsync(file, document.FileContent);
            }
            finally
            {
                this.IsEnabled = true;
                Preloader.IsActive = false;
            }
        }
    }
}