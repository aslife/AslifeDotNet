﻿using System;
using System.Numerics;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using AslifeApiLib.model;
using AslifeSaleTerminal.UI.Primitives;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace AslifeSaleTerminal.UI.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ProductPage : Page
    {
        // private BigInteger? _productCode;
        private ReadPolicy _readPolicy;

        public ProductPage()
        {
            this.InitializeComponent();
            ButtonCreate.Click += ButtonCreateOnClick;
            //TODO: заменить на квалифаер
            ButtonReload.Click += (sender, args) =>
            {
                if (null == _readPolicy)
                    return;
                Load(_readPolicy);
            };
        }

        private async void ButtonCreateOnClick(object sender, RoutedEventArgs e)
        {
            if (null == _readPolicy)
                return;

            Preloader.IsActive = true;
            this.IsEnabled = false;
            try
            {
                BasicResponse<PolicyQualifier> result;
                using (var client = Util.ApiClient)
                {
                    result = await Task.Run(() => client.WriteProduct(new WritePolicy
                    {
                        EmployeeId = Util.AgentEmployeeId,
                        ProductCode = _readPolicy.ProductCode,
                        PolicyId = _readPolicy.PolicyId,
                        PolicyNum = _readPolicy.PolicyNum,
                        PolicyData = ProductForm.GetValues()
                    }));
                }

                if (null != result.Faults && 0 != result.Faults.Count)
                    await new FaultDialog(result.Faults).ShowAsync();
                else
                {
                    var dialog = new ContentDialog
                    {
                        Title = $"{result.Data.PolicyNum} | Успех",
                        Content = $"Полис по успешно записан.\n" +
                                  $"Номер: {result.Data.PolicyNum}, идентификатор: {result.Data.PolicyId}",
                        CloseButtonText = "Ok"
                    };
                    await dialog.ShowAsync();
                    Frame.Navigate(typeof(PolicyPage), result.Data);
                    MainPage.ClearHistory(new PageStackEntry(typeof(ProductListPage), null,
                        new EntranceNavigationTransitionInfo()));
                }
            }
            finally
            {
                this.IsEnabled = true;
                Preloader.IsActive = false;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            ProductForm.Clear();

            var readPolicy = (ReadPolicy) e.Parameter;
            if (null != readPolicy)
                Load(readPolicy);
        }

        public async void Load(ReadPolicy readPolicy)
        {
            Preloader.IsActive = true;
            _readPolicy = null;
            ProductForm.Clear();
            try
            {
                BasicResponse<ProductStruct> result;
                using (var client = Util.ApiClient)
                {
                    result = await Task.Run(() => client.ReadProduct(new ReadPolicy
                    {
                        AgentEmployeeId = Util.AgentEmployeeId, ProductCode = readPolicy.ProductCode, 
                        PolicyId = readPolicy.PolicyId, PolicyNum = readPolicy.PolicyNum
                    }));
                }

                if (null != result.Faults && 0 != result.Faults.Count)
                    await new FaultDialog(result.Faults).ShowAsync();
                else
                    ProductForm.SetFields(result.Data.Entities, result.Data.Dictionaries);

                _readPolicy = readPolicy;
            }
            finally
            {
                Preloader.IsActive = false;
            }
        }
    }
}